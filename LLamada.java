import javax.swing.JOptionPane;

 class Votaciones{
    public int morado, acuamarino, blanco, votosSeccion = 0;
    String partido;
    int vot[]= new int[3];
    int votosSec[]= new int[3];
    static int votosTotal[]= new int[3];
    String observador;
    String secciones[]={"1020","1021","1022","1023","1024"};
    static String part[]= {"Partido MORADO","Partido ACUAMARINO","Partido BLANCO"};
        
   public String registroSecciones(){
        String numSec;
        numSec=(String)JOptionPane.showInputDialog(null,"SELECCIONE NUMERO DE SECCION",
                    "SECCIONES ELECTORALES DE JILOTEPEC", JOptionPane.DEFAULT_OPTION, null, 
                    secciones, secciones[0]);
        return numSec;
    }
   /* Despliega un menu con los partidos disponibles, despues de seleccionar uno
    * retorna el valor seleccionado*/
   public String seleccionarPartido(){
        partido=(String)JOptionPane.showInputDialog(null,"Seleccione partido de su preferencia",
                    "PARTIDOS", JOptionPane.DEFAULT_OPTION, null, part, part[0]);
        return partido;
   }


   public int registroVotos(){
       int x, repetir;
       morado = 0; acuamarino = 0; blanco = 0;
       do{
	    /* Sabemos que el arreglo seleccionarPartidos devuelve un valor de tipo String
	     * por lo que es posible hacer la llamada de dicho metodo desde un swtich case
	     * y evaluarla al instante, estoy sosprendido*/   
            switch (seleccionarPartido()) {
                case "Partido MORADO":
                    this.morado++;
                    break;
                case "Partido ACUAMARINO":
                    this.acuamarino++;
                    break;
                case "Partido BLANCO":
                    this.blanco++;
                    break;
            }
        repetir = Integer.parseInt(JOptionPane.showInputDialog("Ciudadano en la fila? Presiona 1"
				+ "si la respuesta es afirmativa"));
        }while(repetir == 1);

        /* Representa el total de votos por seccion y el arreglo es necesario para poder
	 * hacer la suma de los votos que obtubo cada partido en total.*/
        votosSeccion = morado + acuamarino + blanco;
        int votosSec[]={morado, acuamarino, blanco};
	/* Aqui usamos un super archirrecontra increible e inalcanzable ciclo foreach o 
	 * mejor conocido como el super ciclo for mejorado su funcionamiento es sensillo
	 * primero se coloca el tipo de dato seguido de un identificador y finalmente el 
	 * arreglo a recorrer, juas, juas, juas >:D*/
        for(x = 0; x < part.length; x++){
            System.out.print(part[x] + ":");
            System.out.println(votosSec[x] + " votos");
	    /* Podria decirse que este es un arreglo acumulador ya que los votos obtenidos 
	     * se suman en cada vuelta para saber el numero de votos por partido en total*/
            this.votosTotal[x] += votosSec[x];
        }
        System.out.println("Votaron " + votosSeccion + "  personas en esta seccion" );
        return votosSeccion;
    }
   /* Este metodo no hace mas que recibir nombres de observadores e imprimirlos, sin 
    * embargo se tienen que cumplir dos condiciones primero se afirmar la existencia 
    * del observador y no se admiten mas de 3 observadores*/
   public void registroObservadores(){
       byte x = 0;
       String repetir;
       repetir = JOptionPane.showInputDialog("Existen observadores electorales"
                    + "en la seccion \n en caso afirmativo presione 1");
       while(repetir.equals("1") && x < 3){
            observador = JOptionPane.showInputDialog("Ingresa nombre del observador "
                        + "electoral para la seccion " );
            x++;
            repetir = JOptionPane.showInputDialog("Desea registrar un observador "
                   + "para esta seccion \n en caso afirmativo presione 1");
            System.out.println(observador);
       }
    }
}

public class LLamada{
    static int votosTot=0;
    public static void main(String [] args){
        byte repetir, x;
        Votaciones obj = new Votaciones();
        do{
            System.out.println("Seccion " + obj.registroSecciones());
            System.out.println("OBSERVADORES ELECTORALES");
            obj.registroObservadores();
            votosTot += obj.registroVotos();
            repetir = Byte.parseByte(JOptionPane.showInputDialog("Desea registrar "
                    + "los votos de otra seccion? \n "
                    + "Presione 1 si su respuesta es afirmativa"));
            System.out.println("");
        }while(repetir==1);
         for(x = 0; x < obj.votosTotal.length; x++){
             System.out.print(obj.part[x] + ":");
             System.out.println( obj.votosTotal[x] + " votos");
         }
         System.out.println(" \n TOTAL DE VOTANTES EN JILOTEPEC " + votosTot);
    }
}
