/* Nombre: Raul Hernande Lopez @Neo
 * correo: freeenergy1975@gmail.com
 * miesrcoles 26 de mayo del 2021
 * Tema: Polimorfismo*/

//Clase hija de Vehiculo
public class Principal{
    public static void main(String Neo[]){
      /*CREACION DE UN ARREGLO DE TIPO VEHICULOS, 4 vehiculos*/	    
      Vehiculo misVehiculos[] = new Vehiculo[4];
      /*LLamada a los metodos contructores para inicializar los datos*/
      misVehiculos[0] = new Vehiculo("KP15", "Ford", "b47");
      misVehiculos[1] = new VehiculoTurismo( 4, "6FTD", "Audi", "H53");
      misVehiculos[2] = new VehiculoDeportivo(500, "GR43", "Toyota", "K34");
      misVehiculos[3] = new VehiculoFurgoneta(2000, "H322", "Toyota", "D34");

      /* Uso de ciclo foreach o el super archirrecontra imparable bucle for 
       * mejorado, funciona de la sig manera "Tipo de  dato,  identificador 
       * cual sea y finalmente nombre del  arreglo juas, juas, juas >:D "*/
      for(Vehiculo vehiculos: misVehiculos){
	  /*Una vez que los datos fueron inicializados podemos proceder a    
	   *llamar al metodo mostrar datos que en cada instanciamiento es 
	   *Distinto de acuerdo al caso >:D*/ 
          System.out.print(vehiculos.mostrarDatos());
      }	
    }
}

