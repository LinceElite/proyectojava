/* Autor Hernandez Lopez  Raul @Neo
 * correo: freeenergy1975@gmail.com
 * grupo: 3202   carrera: I.S.C*/

import java.util.Scanner;

public class Excepciones extends Numerico{
   public static void main (String Neo[]){
       Numerico obj = new Numerico();
       int numerador, excepcion;;
       String numero;
       int repetir;
       do {
           System.out.print("Excepciones\n1)Division entre cero\n2)Error de parseo\n" + 
		       "3)Posicion que no existe\nDigita tu opcion: ");
           excepcion = entrada.nextInt();
	   try{
               switch (excepcion){
	          case 1:      
                   System.out.print("\nIngresa un numero para dividir :");
                   numerador = entrada.nextInt();
                   obj.dividir(numerador);
 	          break;

	          case 2:
                   System.out.print("\nIngresa un numero :");
                   numero = entrada.next();
                   obj.parsear(numero);
 	          break;

	          case 3:
                    obj.accederArray();
 	          break;   
               }	   
           }//Fin try
           catch(ArithmeticException errror_aritmetico){
               System.out.print("la division por cero no esta permitida\n");
	       repetir = 1;
           } 
           catch(ArrayIndexOutOfBoundsException error_indexacion){
               System.out.print("posicion fuera de los limites del array\n");
	       repetir = 1;
           }     
           catch(NumberFormatException error_parseo){
               System.out.print("Solo valores numericos\n");
	       repetir = 1;
           }

	   System.out.print("\nPara continuar dijite 1 :");
	   repetir = entrada.nextInt();
       }while(repetir == 1);
   }
}

class Numerico{
    static Scanner entrada = new Scanner(System.in);	
    int enviar;
    int valorNull;
    int array[] = new int[2];
    public float dividir(int numerador){
        return numerador/0;
    }
    public int parsear(String numero){
        enviar = Integer.parseInt(numero);
	return enviar;
    }

    public void accederArray(){
	System.out.print("El valor del elemento es : " + array[3]);
    }
} 
