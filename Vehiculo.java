/* Nombre: Raul Hernande Lopez @Neo
 * correo: freeenergy1975@gmail.com
 * miesrcoles 26 de mayo del 2021
 * Tema: Polimorfismo*/

//clase padre
public class Vehiculo{
   /* Atributos protegidos solo accesibles dentro del mismo 
    * paquete o subclases*/ 	
   protected String matricula;
   protected String marca;
   protected String modelo;

   //Metodo contructor el cual inicializa las variables   
   public Vehiculo(String matricula, String marca, String modelo){
       this.matricula = matricula;
       this.marca = marca;
       this.modelo = modelo;
   }

   public String getMatricula(){
      return matricula;
   }

   public String getMarca(){
      return marca;
   }

   public String getModelo(){
      return modelo;
   }
   /*Metodo que muestra datos*/
   public String mostrarDatos(){
      return "\nMatricula :" + matricula 
	      + "\nMarca :" + marca 
	      + "\nModelo" + modelo + "\n";
   }
}
