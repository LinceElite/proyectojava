/* Autor: Hernandez Lopez Raul @Neo
 * Correo: freeenergy1975@gmail.com
 * Fecha: 11 de junio del 2021
 * Tema: Programa que registra la produccion de cafe por mes de un anio
 * saca el promedio de produccion y determina el mes con mayor y menor
 * produccion*/
import javax.swing.JOptionPane;

public class  RegistroProduccionDeCafe{
    public static void main(String Neo[]){
        controlDeProduccion obj = new controlDeProduccion();
	String numMes[] = {"1", "2", "3","4", "5", "6", "7", "8", "9", "10", "11", "12"};
	String mesSeleccionado;
	int mesSeleccion;
	int repetir;
	int x;

	do{
	     mesSeleccionado = (String)JOptionPane.showInputDialog(null, "seleccione un mes",
	        "seleccion", JOptionPane.DEFAULT_OPTION, null, numMes, numMes[0]);
	     mesSeleccion = Integer.parseInt(mesSeleccionado);
	     obj.registrarProduccion(mesSeleccion);

              repetir =Integer.parseInt(JOptionPane.showInputDialog("Si deseas registrar la produccion" 
			      + " de otro mes digita 1"));

	}while(repetir == 1);
        obj.ordenarValores();
	
	System.out.print("\nEl mes o meses con mayor produccion son los siguientes: ");
	obj.compararYMostrar(11);
	System.out.print("\n\nMientras que los meses con menos produccion son los siguientes :");
	obj.compararYMostrar(0);
	System.out.print("\n\nPromedio anual: " + obj.promedio);
        	
    }
}

class controlDeProduccion{
        static float produccionMes[] = new float[12];
        String nomMes[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo"
                , "junio", "julio", "agosto", "septiembre", "Octubre"
                , "Noviembre", "Diciembre" };
	float cambioPos;
	float promedio;
	String cambioPosNom;
	int x, y;

	protected void registrarProduccion(int mesSeleccion) {
	   produccionMes[mesSeleccion-1] = Float.parseFloat(JOptionPane.showInputDialog("Produccion" 
			   + " en kg de cafe"));
	}
        protected void ordenarValores(){
	for(x = 0; x <= 6; x++)	
	    for(y = 0; y < (produccionMes.length)-1; y++){
		if (produccionMes[y] > produccionMes[y+1]){
		    cambioPos = produccionMes[y+1];
		    produccionMes[y + 1] = produccionMes[y];
		    produccionMes[y] = cambioPos;

		    cambioPosNom = nomMes[y+1];
		    nomMes[y+1] = nomMes[y];
		    nomMes[y] = cambioPosNom;
		}
	    }
	} 
       protected void compararYMostrar(int comparar){
	   float prePromedio = 0;    
	   int x;
 	   for(x = 0; x < produccionMes.length; x++){
	      prePromedio += produccionMes[x];
              if(produccionMes[comparar] == produccionMes[x]){
                 System.out.print("\n" + nomMes[x]+ " produccion :" 
				 + produccionMes[x] );
              }      
           }
	   promedio = prePromedio/12;
       } 
}

