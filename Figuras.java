/*Autor: Hernandez Lopez Raul @Neo
 *Catedratic@: Yadira Jimenez Perez
 *Correo: freeenrgy1975@gmail.com
 *Tema: Clases, metodos abstractos e intefaces*/

import java.util.Scanner;

/*Creacion de la clase madre abstracta Figura2D*/
abstract class Figura2D{
   Scanner entrada = new Scanner(System.in);	
   protected double area;	
   abstract double calcularArea();//Metodo abstracto.
}

interface Figura3D{//Declaracion de la interfaz Figura3D.
   final double PI = 3.14159;//Definicion de la constante PI.	
   double calcularVolumen();//Metodo abstracto y publico sin definicion.
}

class Cuadrado extends Figura2D{
   float lado;
   @Override//Reescripcion del metodo area.
   public double calcularArea(){
       System.out.print("\nLado de la figura :");
       lado = entrada.nextFloat();  
       area = (lado * lado);
       return area;
   }
} 

class Triangulo extends Figura2D{
   float base, altura;	
   @Override//Reescripcion del metodo area.
   public double calcularArea(){
       System.out.print("\n\nBase del triangulo :");     
       base = entrada.nextFloat();
       System.out.print("Altura del triangulo :");
       altura = entrada.nextFloat();
       area = ((base *altura)/2);
       return area;
   }
}

class Esfera extends Figura2D implements Figura3D{
    float radio;
    double volumen;
    @Override
    public double calcularArea(){//Reescripcion del metodo area.    
	System.out.print("\n\nRadio de la esfera :");    
	radio = entrada.nextFloat();
        area = ((4*PI)*(radio*radio));
	return area;
    }

    @Override//Reescripcion del metodo volumen.
    public double calcularVolumen(){
	volumen = (((4 * PI *(radio * radio * radio))/3));
	return volumen;
    }
}

class Figuras{
    public static void main (String Neo[]){
	/*Creacion de los objetos para toda y cada una de las 
	 * clases de figuras que tenemos*/    
        Cuadrado cuadrado = new Cuadrado();
	Triangulo triangulo = new Triangulo();
	Esfera esfera = new Esfera();
        /*LLamada a los metodos*/
	System.out.print("El area del cuadrado es :" + cuadrado.calcularArea());
	System.out.print("El area del triangulo es :" + triangulo.calcularArea());
	System.out.print("El area de la esfera es :" + esfera.calcularArea()); 
  	System.out.print("\nMientras que  el volumen es :" + esfera.calcularVolumen() + "\n");

    }
}
