/* Nombre: Raul Hernande Lopez @Neo
 * correo: freeenergy1975@gmail.com
 * miesrcoles 26 de mayo del 2021
 * Tema: Polimorfismo*/

//Clase hija de Vehiculo
public class VehiculoTurismo extends Vehiculo{
   //Atributo solo accesible dentro de la clase	
   private int numPuertas;

   /* Creacion de un contructo que como bien se menciono anteriormente
    * sirven para inicializar un objeto*/
   public VehiculoTurismo(int numPuertas, String matricula, String marca, String modelo){
      super(matricula, marca, modelo);//Quiere decir que los valores ya estan inicializados
      this.numPuertas = numPuertas;
   }
   //Ya que el atributo numPuertas es privado creamos un metodo publico que retorna su valor
   public int getnumPuertas(){
      return numPuertas;
   }
   /* en esta parte se sobre escribe el metodo mostrar resultados debido a que tenemos un 
    * dato adccional que mostrar*/
   @Override
   public String mostrarDatos(){
      return "\nMatricula :" + matricula 
	      + "\nMarca :" + marca 
	      + "\nModelo" + modelo
	      + "\nNumero de puestas : " + numPuertas + "\n";
   }
}
