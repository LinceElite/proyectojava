import javax.swing.JOptionPane;

public class ComputoVotos extends Votaciones {

    static int votosTot=0;

    static void ordenarVotos(){
	 Votaciones obj = new Votaciones();
         int x, y, cambioPos;
	 String cambioPart;
	 for(x = 0; x < 6; x++){
	     for(y = 0; y < ((obj.votosTotal.length)-1); y++){
	         if( obj.votosTotal[y] > obj.votosTotal[y+1]){

		    cambioPos = obj.votosTotal[y+1];
		    obj.votosTotal[y+1] = obj.votosTotal[y];
		    obj.votosTotal[y] = cambioPos;
			
		    cambioPart = obj.part[y+1];
		    obj.part[y+1] = obj.part[y];
		    obj.part[y] = cambioPart;
		 }
	     }
	 }
    }

    static void imprimirResultados(){
       Votaciones obj = new Votaciones();
       if(obj.votosTotal[2] > obj.votosTotal[1] && obj.votosTotal[2] > obj.votosTotal[0]){
           System.out.print("\nLa Victoria corresponde al partido :" + obj.part[2] 
			   + " Felicidades\n" );
       }
       else if(obj.votosTotal[2] == obj.votosTotal[1] && obj.votosTotal[1] > obj.votosTotal[0]){
           System.out.print("\nHay un empate entre el partido :" + obj.part[2] +" y el partido" 
			   + obj.part[1] + "\n");
       }
       else{
       	   System.out.print("\nHay un empate triple.\n");
       }
    }


    public static void main(String [] args){
        byte repetir, x;
        Votaciones obj = new Votaciones();
        do{
            System.out.println("Seccion " + obj.registroSecciones());
            System.out.println("OBSERVADORES ELECTORALES");
            obj.registroObservadores();
            votosTot += obj.registroVotos();
            repetir = Byte.parseByte(JOptionPane.showInputDialog("Desea registrar "
                    + "los votos de otra seccion? \n "
                    + "Presione 1 si su respuesta es afirmativa"));
            System.out.println("");
        }while(repetir==1);
	ordenarVotos();
         for(x = 0; x < obj.votosTotal.length; x++){
             System.out.print(obj.part[x] + ":");
             System.out.println( obj.votosTotal[x] + " votos");
         }
         System.out.println(" \n TOTAL DE VOTANTES EN JILOTEPEC " + votosTot);
	imprimirResultados();
    }
}

